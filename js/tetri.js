/*
The MIT License (MIT)

Copyright (c) 2015 Martin Parent

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


window.onload = GameInit;

//-----------------------------------------
// Constants
var X=0,Y=1;
var BLOCKANCHOR=[2,1];
var GAMESIZE=[480,853];
var HORIZONTALASPECT = GAMESIZE[Y]/GAMESIZE[X];
var BLOCKCELLS = 16;
var BLOCKSIDE = 4;
var BLOCKSIZE=32;
var GRIDSIZE=[10,20];
var GRIDPOS=[70,128];
var NBBLOCKS=7;
var PITCH = GRIDSIZE[X];
var HZ = 20;
var TIMING = 1/HZ;
var SCOREPERLINE = 100;
var SCOREPERADDITIONNALLINE = 20;
var MAXLEVEL = 11;
var GRIDASPECT = (GRIDSIZE[X]*BLOCKSIZE)/(GRIDSIZE[Y]*BLOCKSIZE);
var _elemColors = [
	[1.0,  1.0,  0.0,  1.0],    // Yellow O	       
	[0.0,  1.0,  1.0,  1.0],    // Cyan I
	[0.0,  1.0,  0.0,  1.0],    // Green S
	[1.0,  0.0,  0.0,  1.0],    // Red Z
	[1.0,  0.4,  0.0,  1.0],    // Orange L	    
	[0.1,  0.1,  0.9,  1.0],    // Blue J	
	[0.7,  0.0,  0.8,  1.0],	// Purple T
	[1.0,  1.0,  1.0,  1.0]
];

//-----------------------------------------
// App vars
var _gridCanvas = null;
var _gridCtx = null;
var _gameCanvas = null;
var _gameCtx = null;
var _webGlSupported = false;

// WebGL vars
var gl = null;
var _shaderProgram = null;
var _mvMatrix = null;
var _perspectiveMatrix = null;
var _cubeVerticesBuffer = null;
var _cubeIndexBuffer = null;
var _cubeVerticesNormalBuffer = null;
var _cubeVerticesTexCoordBuffer = null;

var _vertexPositionAttribute = null;
var _vertexNormalAttribute = null;
var _textureCoordAttribute = null;
var _samplerLocation = null;


//-----------------------------------------
// Events


//-----------------------------------------
// Assets
var _assetsFolder = "img";
var _blocksManifest=
[
	"block1.png",
	"block2.png",
	"block3.png",
	"block4.png",
	"block5.png",
	"block6.png",
	"block7.png",
	"block8.png"
];
var _generalManifest=
[	
	"bkg.png"
];
var _assets = {};
var _textures = {};
var _totalImagesLoaded = 0;
var _totalImagesToLoad = 0;

//-----------------------------------------
// Game vars
var _paused = false;
var _freezed = false;
var _freezeTimer = 0;
var _tetrominos = null;
var _grid = //Init to 200 spaces (10x20)
[
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
];

var _currentBlock = -1;
var _currentRotation = 0;
var _currentPosition = [0,0];

var _currentLevel = 1;
var _currentScore = 0;
var _currentLines = 0;
var _highScore = 0;

var _ticks = 0;

var _fallDown = false;
var _sideToMove = 0;
var _rotate = false;

var _linesToRemove = [];

var _useWebGL = true;


//-----------------------------------------
// Time management & Delta time
var _lastUpdate = 0;
var _currentDt = 0;
var _totalDt = 0;

//-----------------------------------------
function GameInit()
{
	_gridCanvas = document.getElementById("gridCanvas");
	_gridCanvas.width = GRIDSIZE[X]*BLOCKSIZE;
	_gridCanvas.height = GRIDSIZE[Y]*BLOCKSIZE;	
	_gameCanvas = document.getElementById("gameCanvas");
	_gameCanvas.width = GAMESIZE[X];
	_gameCanvas.height = GAMESIZE[Y];
	_gameCtx = _gameCanvas.getContext('2d');

	if (_useWebGL)
	{
		DetectWebGLSupport();
	}

	if (_useWebGL && _webGlSupported)
	{
		//Init WebGL Stuff
		gl = _gridCanvas.getContext("webgl") || _gridCanvas.getContext("experimental-webgl");

		if (gl) {
		    gl.clearColor(0.0, 0.0, 0.0, 1.0);
		    gl.clearDepth(1.0);                      // Set clear color to black, fully opaque
		    gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
		    gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things
		    //gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);      // Clear the color as well as the depth buffer.

		    InitShaders();
		    InitBuffers();
		}
	}else{
		_gridCtx = _gridCanvas.getContext('2d');
	}

	window.addEventListener("keydown",OnKeyDown);
	window.addEventListener("touchstart",OnTouch);

	//Load the Tetrominos JSON first
	LoadJSON("data/tetrominos",OnTetrominosJsonReceived);
}

function OnTetrominosJsonReceived(data)
{
	_tetrominos = JSON.parse(data);

	_startPos = _tetrominos.startPos;
	_tetrominos = _tetrominos.tetrominos;

	_totalImagesToLoad = _blocksManifest.length + _generalManifest.length;

	LoadBlockImages();
	LoadGeneralImages();
}

function GameStart()
{
	//Draw background on game canvas	

	window.requestAnimationFrame(Update);
}

function LoadBlockImages()
{
	for (var i = 0; i < _blocksManifest.length; ++i)
	{
		LoadImage(_assetsFolder+"/"+_blocksManifest[i],OnBlockImageLoaded);
	}
}

function LoadGeneralImages()
{
	for (var i = 0; i < _generalManifest.length; ++i)
	{
		LoadImage(_assetsFolder+"/"+_generalManifest[i],OnGeneralImageLoaded);
	}
}

function OnImageLoaded(imageBlob, filename, img)
{
	img = img || new Image();
	img.src = URL.createObjectURL(imageBlob);
	var nameLength = filename.lastIndexOf('.') - filename.lastIndexOf('/') - 1;
	var imgName = filename.substr(filename.lastIndexOf('/')+1,nameLength);
	_assets[imgName] = img;

	return imgName;
}

function OnBlockImageLoaded(imageBlob,filename)
{
	var imgContainer = new Image(BLOCKSIZE,BLOCKSIZE);

	//Create a texture from the image to use with WebGL if supported.
	if (_useWebGL && _webGlSupported)
	{
		var imgName = OnImageLoaded(imageBlob,filename,imgContainer);
		var img = _assets[imgName];

		img.onload = function()
		{
			var texture = GetWebGLTextureFromImage(img);

			_textures[imgName] = texture;
		};		
	}	

	if (++_totalImagesLoaded >= _totalImagesToLoad)
	{
		GameStart();
	}
}

function OnGeneralImageLoaded(imageBlob,filename)
{
	var imgContainer = new Image(GAMESIZE[X],GAMESIZE[Y]);

	OnImageLoaded(imageBlob, filename, imgContainer);

	if (++_totalImagesLoaded >= _totalImagesToLoad)
	{
		GameStart();
	}
}

function Update()
{
	window.requestAnimationFrame(Update);

	//Draw background
	_gameCtx.drawImage(_assets["bkg"],0,0,GAMESIZE[X],GAMESIZE[Y]);

	//Draw game stats
	_gameCtx.fillStyle = "#FFFFFF";
	_gameCtx.font = "bold 14px Verdana";

	_gameCtx.fillText("H.Score:", 30, 40);
	_gameCtx.fillText(_highScore, 30, 55);
	_gameCtx.fillText("Score:", 30, 80);
	_gameCtx.fillText(_currentScore, 30, 95);

	_gameCtx.fillText("Level:", 385, 40);
	_gameCtx.fillText(_currentLevel, 385, 55);
	_gameCtx.fillText("Lines:", 385, 80);
	_gameCtx.fillText(_currentLines, 385, 95);

	//Get DT
	var now = Date.now();
    _currentDt = (now - _lastUpdate)/1000;
    _lastUpdate = now;

    if (_paused||_freezed)
    {
    	if (_freezed)
    	{
    		_freezeTimer -= _currentDt;
    		if (_freezeTimer <= 0)
    		{
    			RemoveLines();
    		}
    		_ticks = 0;
    		_totalDt = 0;
    	}else{
    		RenderGrid();
    		return;
    	}    	
    } 

    _totalDt+=_currentDt;

    if (_totalDt >= TIMING)
    {
    	_totalDt = Clamp(_totalDt,0,0.1);
    	_ticks += Math.floor(_totalDt / TIMING);
    	_totalDt = _totalDt % TIMING;

    	UpdateGrid();
    	if (_useWebGL && _webGlSupported)
    	{
    		RenderGrid3D();
    		//RenderGrid2D();
    	}else{
    		RenderGrid2D();	
    	}
    	
    }
}

function UpdateGrid()
{
	if (_currentBlock == -1)
	{
		_currentBlock = Math.floor(Math.random()*_tetrominos.length);
		_currentRotation = 0;
		_currentPosition[X] = _startPos[X];
		_currentPosition[Y] = _startPos[Y];

		var lose = false;
		for (var j = 0; j < BLOCKSIDE && !lose; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

				if(GetGridCell(x,y)!=0)
				{
					lose = true;
					break;
				}				
			}			
		}
		if (lose)
		{
			ResetGame();
			return;
		}
	}

	var moveBlockDownOnce = (_ticks >= MAXLEVEL-_currentLevel);

	if (_rotate)
	{
		var initialRotation = _currentRotation;

		//Rotate block
		_currentRotation = (_currentRotation+1)%_tetrominos[_currentBlock].rotations.length;

		//Move Block
		for (var j = 0; j < BLOCKSIDE; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];
				
				if(!(InBounds(x,0,GRIDSIZE[X]-1) && InBounds(y,0,GRIDSIZE[Y]-1) && GetGridCell(x,y)==0))
				{
					_rotate = false;
					break;
				}
			}			
		}

		if (!_rotate)
		{
			_currentRotation = initialRotation;
		}

		_rotate = false;
	}

	if (_sideToMove!=0)
	{
		canMove = true;
		//Move Block
		for (var j = 0; j < BLOCKSIDE && canMove; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];
				
				if(!(InBounds(x+_sideToMove,0,GRIDSIZE[X]-1) && GetGridCell(x+_sideToMove,y)==0))
				{
					canMove = false;					
					break;
				}
			}			
		}

		if (canMove)
		{
			_currentPosition[X] += _sideToMove;
		}

		_sideToMove = 0;
	}

	if (_fallDown)
	{
		_fallDown = false;

		var minRowsToMove = GRIDSIZE[Y];
		
		//Check each cell of the block for the minimum fall value possible
		for (var j = 0; j < BLOCKSIDE; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

				var rowsToMove = 0;
				do{
					rowsToMove++;
				}while(InBounds(y+rowsToMove,0,GRIDSIZE[Y]-1) && GetGridCell(x,y+rowsToMove)==0);
				rowsToMove--;
				if (rowsToMove < minRowsToMove)
				{
					minRowsToMove = rowsToMove;
				}
			}			
		}
		if (minRowsToMove>0)
		{
			_ticks = 0;
			_currentPosition[Y]+=minRowsToMove;
			return;
		}
		
	}

	if (moveBlockDownOnce)
	{
		_ticks = 0;

		var commit = false;
		for (var j = 0; j < BLOCKSIDE && !commit; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

				if(!(InBounds(y+1,0,GRIDSIZE[Y]-1) && GetGridCell(x,y+1)==0))
				{
					commit = true;
					break;
				}				
			}			
		}
		if (commit)
		{
			CommitBlock();
		}else{
			_currentPosition[Y]++;
		}
	}
}

function RenderGrid2D()
{
	_gridCtx.clearRect(0,0,GRIDSIZE[X]*BLOCKSIZE,GRIDSIZE[Y]*BLOCKSIZE);
	for (var j = 0; j < GRIDSIZE[Y]; ++j)
	{
		for (var i = 0; i < GRIDSIZE[X]; ++i)
		{
			var gridCell = GetGridCell(i,j);
			if (gridCell>0)
			{
				_gridCtx.drawImage(_assets["block"+gridCell],i*BLOCKSIZE,j*BLOCKSIZE, BLOCKSIZE, BLOCKSIZE);
			}
		}
	}
	if (_currentBlock >= 0)
	{
		for (var j = 0; j < BLOCKSIDE; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

				_gridCtx.drawImage(_assets["block"+(_currentBlock+1)],x*BLOCKSIZE,y*BLOCKSIZE, BLOCKSIZE, BLOCKSIZE);
			}
		}
	}
}

function RenderGrid3D()
{
	// Clear the canvas before we start drawing on it.

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Establish the perspective with which we want to view the
	// scene. Our field of view is 45 degrees, we pass the aspect ratio of the grid and we only want to see objects between 0.1 units
	// and 100 units away from the camera.
	_perspectiveMatrix = makePerspective(45, GRIDASPECT, 0.1, 100.0);

	// Set the drawing position to the "identity" point, which is
	// the center of the scene.
	loadIdentity();

	// Now move the drawing position a bit to where we want to start
	// drawing the cubes.
	mvTranslate([-9.0, 18.8, -50.0]);	

	// Save the current matrix, then rotate before we draw.

	for (var j = 0; j < GRIDSIZE[Y]; ++j)
	{
		for (var i = 0; i < GRIDSIZE[X]; ++i)
		{
			var gridCell = GetGridCell(i,j);
			if (gridCell>0)
			{
				mvPushMatrix();				
				mvTranslate([i*2,-j*2, 0]);

				// Draw the cube by binding the array buffer to the cube's vertices
				// array, setting attributes, and pushing it to GL.

				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesBuffer);
				gl.vertexAttribPointer(_vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

				// Set the texture coordinates attribute for the vertices.  
				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesTexCoordBuffer);
				gl.vertexAttribPointer(_textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesNormalBuffer);
				gl.vertexAttribPointer(_vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

				// Set the color of the cube
				//gl.uniform3f(_elemColorLocation, _elemColors[gridCell-1][0],_elemColors[gridCell-1][1],_elemColors[gridCell-1][2]);

				gl.activeTexture(gl.TEXTURE0);
				gl.bindTexture(gl.TEXTURE_2D, _textures["block"+gridCell]);
				gl.uniform1i(_samplerLocation,0);

				// Draw the cube.

				gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _cubeIndexBuffer);
				setMatrixUniforms();
				gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

				//gl.drawArrays(gl.LINE_STRIP,0,_cubeVerticesBuffer.length/3);
				//gl.flush();

				// Restore the original matrix

				mvPopMatrix();
				//_gridCtx.drawImage(_assets["block"+gridCell],i*BLOCKSIZE,j*BLOCKSIZE, BLOCKSIZE, BLOCKSIZE);
			}
		}
	}
	if (_currentBlock >= 0)
	{
		for (var j = 0; j < BLOCKSIDE; ++j)
		{			
			for (var i = 0; i < BLOCKSIDE; ++i)
			{
				if (!IsValidBlockCell(i,j)) continue;

				var x = _currentPosition[X]+i-BLOCKANCHOR[X];
				var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

				mvPushMatrix();				
				mvTranslate([x*2,-y*2, 0]);

				// Draw the cube by binding the array buffer to the cube's vertices
				// array, setting attributes, and pushing it to GL.

				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesBuffer);
				gl.vertexAttribPointer(_vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

				// Set the texture coordinates attribute for the vertices.  
				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesTexCoordBuffer);
				gl.vertexAttribPointer(_textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

				gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesNormalBuffer);
				gl.vertexAttribPointer(_vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

				// Set the color of the cube
				//gl.uniform3f(_elemColorLocation, _elemColors[_currentBlock][0],_elemColors[_currentBlock][1],_elemColors[_currentBlock][2]);

				gl.activeTexture(gl.TEXTURE0);
				gl.bindTexture(gl.TEXTURE_2D, _textures["block"+(_currentBlock+1)]);
				gl.uniform1i(_samplerLocation,0);

				// Draw the cube.

				gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _cubeIndexBuffer);
				setMatrixUniforms();
				gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

				// Restore the original matrix

				mvPopMatrix();
				//_gridCtx.drawImage(_assets["block"+(_currentBlock+1)],x*BLOCKSIZE,y*BLOCKSIZE, BLOCKSIZE, BLOCKSIZE);
			}
		}
	}
  
	
}

function RotatePiece()
{
	if (_paused||_freezed) return;

	_rotate = true;
}

function MovePiece(direction)
{
	if (_paused||_freezed) return;

	_sideToMove = direction;
}

function FallDownPiece()
{
	if (_paused||_freezed) return;

	_fallDown = true;
}

function Pause()
{
	_paused = !_paused;
}

function CommitBlock()
{
	for (var j = 0; j < BLOCKSIDE; ++j)
	{			
		for (var i = 0; i < BLOCKSIDE; ++i)
		{
			if (!IsValidBlockCell(i,j)) continue;

			var x = _currentPosition[X]+i-BLOCKANCHOR[X];
			var y = _currentPosition[Y]+j-BLOCKANCHOR[Y];

			SetGridCell(x,y,_currentBlock+1);		
		}			
	}

	_currentBlock = -1;

	MarkLinesForRemove();
}

function MarkLinesForRemove()
{
	var totalLines = 0;
	for (var j = 0; j < GRIDSIZE[Y]; ++j)
	{
		var haveline = true;
		for (var i = 0; i < GRIDSIZE[X]; ++i)
		{
			if (GetGridCell(i,GRIDSIZE[Y]-1-j) == 0)
			{
				haveline = false;
				break;
			}
		}
		if (haveline)
		{
			totalLines++;
			_linesToRemove.push(GRIDSIZE[Y]-1-j);
			for (var i = 0; i < GRIDSIZE[X]; ++i)
			{
				SetGridCell(i,GRIDSIZE[Y]-1-j,8)
			}
		}
	}
	if (totalLines>0)
	{
		_freezed = true;
		_freezeTimer = 0.3;		
	}
}

function RemoveLines()
{
	while (_linesToRemove.length)
	{
		var consecutiveLines = 1;
		
		var j = _linesToRemove.length-1;
		while (j-1 >= 0)
		{
			if (_linesToRemove[j]+1 == _linesToRemove[j-1])
			{
				consecutiveLines++;
			}else{
				break;
			}
			j--;
		}
		_currentScore += SCOREPERLINE*consecutiveLines + SCOREPERADDITIONNALLINE*((consecutiveLines-1)*(consecutiveLines-1));

		if (_currentScore > _highScore)
		{
			_highScore = _currentScore;
		}

		_currentLines += consecutiveLines;

		for (var j = _linesToRemove[_linesToRemove.length-consecutiveLines]; j >= 0; --j)
		{
			for (var i = 0; i < GRIDSIZE[X]; ++i)
			{
				SetGridCell(i,j,GetGridCell(i,j-consecutiveLines));
			}
		}

		_linesToRemove.length -= consecutiveLines;		
	}

	_currentLevel = Math.floor(_currentLines/10)+1;

	_freezed = false;
}

function ResetGame()
{
	//Clear grid
	for (var i = 0; i < _grid.length; ++i)
	{
		_grid[i] = 0;
	}

	_currentBlock = -1;

	_currentScore = 0;
	_currentLines = 0;
	_currentLevel = 1;

	_fallDown = false;
	_rotate = false;
	_sideTomove = 0;

	_ticks = 0;
}

function GetGridCell(x,y)
{	
	if (!InBounds(x,0,GRIDSIZE[X]-1) || !InBounds(y,0,GRIDSIZE[Y]-1))
		return 0;
	return _grid[y*PITCH+x];
}

function SetGridCell(x,y,value)
{
	_grid[y*PITCH+x] = value;
}

function GetBlockCell(block,rotation,x,y)
{
	return _tetrominos[block].rotations[rotation][y*BLOCKSIDE+x];
}
function IsValidBlockCell(x,y)
{
	return GetBlockCell(_currentBlock,_currentRotation,x,y) > 0;
}

function LoadJSON(filename,callback) 
{
	var xobj = new XMLHttpRequest();
	if (xobj.overrideMimeType)
		xobj.overrideMimeType("application/json");
	xobj.open('GET', filename+'.json', true);
	xobj.onreadystatechange = function () 
	{
		if (xobj.readyState == 4 && xobj.status == "200") 
		{
			// .open will NOT return a value but simply returns undefined in async mode so use a callback
			callback(xobj.responseText);
		}
	}
	xobj.send(null);
}

function LoadImage(filename,callback)
{
	var extension = filename.substr(filename.lastIndexOf('.')+1);
	var xobj = new XMLHttpRequest();
	//if (xobj.overrideMimeType)
	//	xobj.overrideMimeType("image/"+extension);
	xobj.open('GET', filename, true);
	xobj.responseType = "blob";
	xobj.onload = function () 
	{
		if (xobj.readyState == 4 && xobj.status == "200") 
		{
			// .open will NOT return a value but simply returns undefined in async mode so use a callback
			callback(xobj.response, filename);
		}
	}
	xobj.send(null);
}

var KEYS = {
	W: 87,
	A: 65,
	B: 66,
	S: 83,
	D: 68,
	UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39,
    ENTER: 13,
    ESCAPE: 27,     
    SPACE: 32   
};
function OnKeyDown(event)
{
	switch (event.keyCode)
	{
		case KEYS.W:
		case KEYS.UP:
		case KEYS.SPACE:
			RotatePiece();
		break;

		case KEYS.S:
		case KEYS.DOWN:
		case KEYS.ENTER:
			FallDownPiece();
		break;

		case KEYS.A:
		case KEYS.LEFT:
			MovePiece(-1);
		break;

		case KEYS.D:
		case KEYS.RIGHT:
			MovePiece(1);
		break;

		case KEYS.ESCAPE:
			Pause();
		break;

		case KEYS.B:
			ResetGame();
		break;
	}
}

function OnTouch(event)
{

}

function Clamp(value,min,max)
{
	return (value<min)?min:((value>max)?max:value);
}

function InBounds(value,min,max)
{
	return (Clamp(value,min,max) == value);
}

//---------------------------------------
// WebGL Stuff

function DetectWebGLSupport()
{
	try {
		var canvas = document.getElementById("gridCanvas");			
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;			
		gl.clearColor(1.0, 0.0, 0.0, 1.0);
		gl.enable(gl.DEPTH_TEST);
		_webGlSupported = true;

	} catch(e) {}	
}

function InitShaders() {
	var fragmentShader = GetShader(gl, "shader-fs");
	var vertexShader = GetShader(gl, "shader-vs");

	// Create the shader program

	_shaderProgram = gl.createProgram();
	gl.attachShader(_shaderProgram, vertexShader);
	gl.attachShader(_shaderProgram, fragmentShader);
	gl.linkProgram(_shaderProgram);

	// If creating the shader program failed, alert

	if (!gl.getProgramParameter(_shaderProgram, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	}

	gl.useProgram(_shaderProgram);

	_vertexPositionAttribute = gl.getAttribLocation(_shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(_vertexPositionAttribute);

	_vertexNormalAttribute = gl.getAttribLocation(_shaderProgram, "aVertexNormal");
	gl.enableVertexAttribArray(_vertexNormalAttribute);

	_textureCoordAttribute = gl.getAttribLocation(_shaderProgram, "aTextureCoord");
	gl.enableVertexAttribArray(_textureCoordAttribute);

	_samplerLocation = gl.getUniformLocation(_shaderProgram, "uSampler");
}

function GetShader(gl, id) {
	var shaderScript, theSource, currentChild, shader;

	shaderScript = document.getElementById(id);

	if (!shaderScript) {
		return null;
	}

	theSource = "";
	currentChild = shaderScript.firstChild;

	while(currentChild) {
		if (currentChild.nodeType == 3) {
			theSource += currentChild.textContent;
		}

		currentChild = currentChild.nextSibling;
	}

	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		// Unknown shader type
		return null;
	}

	gl.shaderSource(shader, theSource);

	// Compile the shader program
	gl.compileShader(shader);  

	// See if it compiled successfully
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {  
		alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));  
		return null;  
	}

	return shader;
}


function InitBuffers() {
	_cubeVerticesBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesBuffer);

	var cubeVertices = [
		// Front face
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0,  1.0,  1.0,
		-1.0,  1.0,  1.0,

		// Back face
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		1.0,  1.0, -1.0,
		1.0, -1.0, -1.0,

		// Top face
		-1.0,  1.0, -1.0,
		-1.0,  1.0,  1.0,
		1.0,  1.0,  1.0,
		1.0,  1.0, -1.0,

		// Bottom face
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,

		// Right face
		1.0, -1.0, -1.0,
		1.0,  1.0, -1.0,
		1.0,  1.0,  1.0,
		1.0, -1.0,  1.0,

		// Left face
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0
	];

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertices), gl.STATIC_DRAW);

	_cubeVerticesNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesNormalBuffer);

	var vertexNormals = [
		// Front
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		// Back
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		// Top
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,

		// Bottom
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Right
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		// Left
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0
	];

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);

	_cubeVerticesTexCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, _cubeVerticesTexCoordBuffer);

	var cubeVerticesTexCoord = [
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Back
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Top
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
	];

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVerticesTexCoord), gl.STATIC_DRAW);

	_cubeIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _cubeIndexBuffer);	
	
	var cubeVertexIndices = [
		0,  1,  2,      0,  2,  3,    // front
		4,  5,  6,      4,  6,  7,    // back
		8,  9,  10,     8,  10, 11,   // top
		12, 13, 14,     12, 14, 15,   // bottom
		16, 17, 18,     16, 18, 19,   // right
		20, 21, 22,     20, 22, 23    // left
	];

	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
}

function GetWebGLTextureFromImage(image) {
	var texture = gl.createTexture();

	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
	gl.generateMipmap(gl.TEXTURE_2D);
	gl.bindTexture(gl.TEXTURE_2D, null);

	return texture;
}

//
// Matrix utility functions
//

function loadIdentity() {
	_mvMatrix = Matrix.I(4);
}

function multMatrix(m) {
	_mvMatrix = _mvMatrix.x(m);
}

function mvTranslate(v) {
	multMatrix(Matrix.Translation($V([v[0], v[1], v[2]])).ensure4x4());
}

function setMatrixUniforms() {
	var pUniform = gl.getUniformLocation(_shaderProgram, "uPMatrix");
	gl.uniformMatrix4fv(pUniform, false, new Float32Array(_perspectiveMatrix.flatten()));

	var mvUniform = gl.getUniformLocation(_shaderProgram, "uMVMatrix");
	gl.uniformMatrix4fv(mvUniform, false, new Float32Array(_mvMatrix.flatten()));

	var normalMatrix = _mvMatrix.inverse();
	normalMatrix = normalMatrix.transpose();
	var nUniform = gl.getUniformLocation(_shaderProgram, "uNormalMatrix");
	gl.uniformMatrix4fv(nUniform, false, new Float32Array(normalMatrix.flatten()));
}

var _mvMatrixStack = [];

function mvPushMatrix(m) {
  if (m) {
    _mvMatrixStack.push(m.dup());
    _mvMatrix = m.dup();
  } else {
    _mvMatrixStack.push(_mvMatrix.dup());
  }
}

function mvPopMatrix() {
  if (!_mvMatrixStack.length) {
    throw("Can't pop from an empty matrix stack.");
  }
  
  _mvMatrix = _mvMatrixStack.pop();
  return _mvMatrix;
}

function mvRotate(angle, v) {
  var inRadians = angle * Math.PI / 180.0;
  
  var m = Matrix.Rotation(inRadians, $V([v[0], v[1], v[2]])).ensure4x4();
  multMatrix(m);
}